# Welcome To Simple CDN Project
We launch a sample web site and serve it via a proxy. On the other hand, our client must be routed with BGP protocol to
access web content. In the end, we made some tuning on the proxy server to improve performance and having logs.

This Project consist of 5 challanges 
- [ ] - Step 1 :  On Webserver, install a simple website with dynamic and static contact. You are free to use any web server. 
- [ ] - Step 2 : Implement BGP routing between the ISP router and EDG server. Now the client can ping the edge server. You can use Bird
(open-source router) to setup it.
- [ ] - Step 3 : Install Nginx as a proxy to forward web requests to the webserver. Now the client must see your sample website. Define
some rules to cache static content
- [ ] - Step 4 : Install ELK for gathering Nginx access/error logs in different elastic search indices. Required Nginx configurations also must
be done at webserver side.
- [ ] - Step 5 : Tuning and extra features:
    * Tune TCP stack on all server base on server functionality
    * Rated (set 10rps) the number of requests that send to the webserver. The extra request must block with status
code 429
    * Write bash scripts for purging one file from cache storage.
    * Add some information in the HTTP response to inform the client about server name, cache status, and response
time.
    * In heavy load, we need a solution to discard 20% of requests on Edge Servers. Create a bash/python to generate
loads, and run it on client.
    * On ELK side, create a retention mechanism to delete logs after X days/hours.


# Project Flow and Requirements 

<h3> Please Read First </h3> 

Note : We have two way

- [ ] - Solution 1 ) Run Vagrant file and provison linux machine and configure all steps manualy 
    * See 5 steps installation methode. 
- [ ] - Solution 2 ) Add ready project from Vagrant box. (all vagrant box will upload to the Cloud)
    * See below commands and follow them. 



<h2> Requirements </h2>

- [ ] - Provisionaning system   : Vagrant 
- [ ] - Operating System        : Ubuntu 20.04(focal)
- [ ] - Webserver and Caching Server : Nginx 
- [ ] - Log Management System : ELK 
- [ ] - BGP Routing : Bird Deamon 
- [ ] - Scripting language : Python 

<h3> Test dynamic url and you see that no chaching we have on dynamic request (client machine) </h3>

```
vagrant ssh client 


root@client:~# curl -I http://e2acloud.local
HTTP/1.1 200 OK
Server: nginx/1.18.0 (Ubuntu)
Date: Sun, 25 Jun 2023 22:42:36 GMT
Content-Type: text/html
Content-Length: 612
Last-Modified: Tue, 21 Apr 2020 14:09:01 GMT
Connection: keep-alive
ETag: "5e9efe7d-264"
Server: cdn.e2acloud.local
X-Response-Time: 0.000
Accept-Ranges: bytes

```
> Now we request a static content (image)

```
root@client:~# curl -I http://e2acloud.local/wp-content/uploads/2023/06/Unlimited-COD-Call-Of-Duty-Wallpapers-4k-Full-HD-Hd-Download-For-Free_-150x150.jpg

HTTP/1.1 200 OK
Server: nginx/1.18.0 (Ubuntu)
Date: Sun, 25 Jun 2023 22:44:45 GMT
Content-Type: image/jpeg
Content-Length: 7387
Connection: keep-alive
Last-Modified: Fri, 23 Jun 2023 20:42:41 GMT
ETag: "649603c1-1cdb"
X-Proxy-Cache: HIT                  # <-------- You Can see caching enabled based on static data  
Server: cdn.e2acloud.local          
X-Response-Time: 0.000
Accept-Ranges: bytes

```
<h2> How package vagrant machine  </h2>

```
$ vagrant package --base client --output client.box
$ vagrant package --base ispr --output ispr.box
$ vagrant package --base webser --output webser.box
$ vagrant package --base edger --output edger.box
$ vagrant package --base logser --output logser.box
```

<h2> How you cand Used Vagrant  machine?  </h2>

```
vagrant box add <name> <filename>.box

$ vagrant box add client client.box
$ vagrant box add ispr ispr.box
$ vagrant box add edger edger.box
$ vagrant box add webser webser.box
$ vagrant box add logser logser.box

```
