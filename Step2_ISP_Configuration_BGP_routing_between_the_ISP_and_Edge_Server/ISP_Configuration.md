<h3>  Implement BGP routing between the ISP router and EDG server  </h3>

Note : In this case we used Bird as a BGP Daemon and Implement BGP routing between ISP and Edge Server 

<h3> Server Info </h3>

- Ip :  192.168.58.11, 192.168.57.11
- Hostname : ispr
- Ram : 2G
- Cpu : 1 Core 

<h3> ISP Router [192.168.58.11] <--------BGP--------> [192.168.58.12] Edge Server </h3>

---------
- [ ] - Step 1 : Pre Installation  Task
- [ ] - Step 2 : Install Bird
- [ ] - Step 3 : Configure and Tune BGP 

<h2> Pre Installation  Task </h2>

> Enable Ip Forwarding and Routing 

```
# echo 1 > /proc/sys/net/ipv4/ip_forward
```
<h2> Install Bird </h2>

```
$ sudo apt install bird
$ sudo systemctl enable bird
$ sudo systemctl start  bird
```
> Config BGP on ISP Side 

```
vim /etc/bird/bird.conf

router id 192.168.58.11;
protocol kernel {
        scan time 10;
        import none;
        export all;   # Actually insert routes into the kernel routing table
}

protocol device {
        scan time 10;
}

protocol direct {
        interface "lo";               # Disable by default
}
protocol static {
        route 192.168.0.2/32  via 192.168.58.12;
        route 192.168.56.12/32  via 192.168.58.12;
        route 192.168.57.0/24  via 192.168.58.11;
        route 192.168.57.11/32  via 192.168.58.12;

}

protocol bgp neighbor_v4_1 {
  local as 65002;
  neighbor 192.168.58.12 as 65001;
  export all;
  import all;
}

```
<h2> See and Check routing status </h2>

```
$sudo birdc show protocols all neighbor_v4_1
```
> You must set this configuration on Edge side in same times 

> output routing status 

```
neighbor_v4_1 BGP      master   up     2023-06-24  Established
  Preference:     100
  Input filter:   ACCEPT
  Output filter:  ACCEPT
  Routes:         1 imported, 5 exported, 0 preferred
  Route change stats:     received   rejected   filtered    ignored   accepted
    Import updates:              1          0          0          0          1
    Import withdraws:            0          0        ---          0          0
    Export updates:              5          0          0        ---          5
    Export withdraws:            0        ---        ---        ---          0
  BGP state:          Established
    Neighbor address: 192.168.58.12
    Neighbor AS:      65001
    Neighbor ID:      192.168.58.12
    Neighbor caps:    refresh enhanced-refresh restart-aware llgr-aware AS4
    Session:          external AS4
    Source address:   192.168.58.11
    Hold timer:       190/240
    Keepalive timer:  45/80

```
