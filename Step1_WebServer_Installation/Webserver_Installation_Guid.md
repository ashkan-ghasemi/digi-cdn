<h3>  Install simple webserver and serve dynamic and static content  </h3>

Note : In this case we used nginx as a webserver and we used wordpress for CMS and beside of wordpress we have static page for serve static content and caching. 

<h3> Server Info </h3>

- Ip :  192.168.56.13
- Hostname : e2acloud.local
- Ram : 2G
- Cpu : 1 Core 

---------
- [ ] - Step 1 : Pre Installation  Task
- [ ] - Step 2 : Install Wordpress and Create Static Page 
- [ ] - Step 3 : Configure and Tune WebServer  
---------
<h3> Step1:  Pre Installation Tasks </h3>

- [ ] -  Install Some Package and update 
```
$ sudo apt update -y 
$ sudo apt upgrade -y 
$ sudo apt install chrony
```
- [ ] -  Configure Hosts and Naming 

```
$ sudo vim /etc/hosts 

192.168.56.13 e2acloud.local e2acloud

```
- [ ] - Set Time and Time Zone 
```
$ sudo systemctl enable chrony 
$ sudo timedatectl set-timezone Asia/Tehran
$ sudo systemctl restart chrony 
```
---------
<h3> Step 2 : Install Wordpress and Create Static Page </h3>

- [ ] - Install LEMP and Some Packages 
```
$ sudo apt-get install nginx php7.4-fpm mariadb-server php php-curl php-mysql php-gd php-intl php-mbstring php-soap php-xml php-xmlrpc php-zip php-fpm -y
```
- [ ] - Enable and Start Services 
```
$ sudo systemctl start nginx
$ sudo systemctl start mariadb
$ sudo systemctl enable  nginx
$ sudo systemctl enable  mariadb
```
- [ ] - Create Mysql Database and Users for wordpress 
```
$ sudo mysql 
MariaDB [(none)]> CREATE DATABASE wpdb;
MariaDB [(none)]> GRANT ALL PRIVILEGES ON wpdb.* TO 'wpuser'@'localhost' IDENTIFIED BY  'securepassword';
MariaDB [(none)]> FLUSH PRIVILEGES;
MariaDB [(none)]> EXIT;
```
- [ ] - Install Wordpress 
```
$ sudo cd /var/www/html
$ sudo wget -c http://wordpress.org/latest.tar.gz
$ sudo tar -xzvf latest.tar.gz
$ sudo mkdir e2acloud.local
$ sudo cp -R wordpress/ /e2acloud.local
$ sudo chown -R www-data:www-data /e2acloud.local
$ sudo chmod -R 775 /e2acloud.local
$ sudo cd e2acloud.local 
$ sudo mv wp-config-sample.php wp-config.php 

```
> Edit this line 
```
$ sudo vim /var/www/html/e2acloud.local/wp-config.php

define('DB_NAME', 'wpdb');
define('DB_USER', 'wpuser');
define('DB_PASSWORD', 'securepassword');

```
- [ ] - Config Nginx for wordpress 

> Create This file and set this configurations 
```
vim /etc/nginx/conf.d/e2acloud.local.conf

server {
        listen 80;
        root /var/www/html/e2acloud.local;
        index  index.php index.html index.htm;
        server_name e2acloud.local www.e2acloud.local;

        error_log /var/log/nginx/e2acloud.local_error.log;
        access_log /var/log/nginx/e2acloud.local_access.log;

        client_max_body_size 100M;
        location / {
                try_files $uri $uri/ /index.php?$args;
        }
        location ~ \.php$ {
                include snippets/fastcgi-php.conf;
                fastcgi_pass unix:/run/php/php7.4-fpm.sock;
                fastcgi_param   SCRIPT_FILENAME $document_root$fastcgi_script_name;
        }
}
```
