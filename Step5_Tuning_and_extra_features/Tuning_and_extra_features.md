<h2> Tuning_and_extra_features </h2>


- [ ] - Tune TCP stack on all server base on server functionality
- [ ] - Rated (set 10rps) the number of requests that send to the webserver. The extra request must block with status
code 429
- [ ] - In heavy load, we need a solution to discard 20% of requests on Edge Servers. Create a bash/python to generate
loads, and run it on client
- [ ] - On ELK side, create a retention mechanism to delete logs after X days/hours.
- [ ] - Write bash scripts for purging one file from cache storage.
- [ ] - Write bash scripts for purging one file from cache storage.

---------

- [ ] - In heavy load, we need a solution to discard 20% of requests on Edge Servers. Create a bash/python to generate
loads, and run it on client

> Python Script For Create Load Test 

```
vim load-test.py

import requests
import time

url = "http://e2acloud.local"
num_requests = 10000

def load_test():
        for i in range(num_requests):
            response = requests.get(url)
            print(f"Request {i+1}: {response.status_code}")
            time.sleep(0)
if __name__ == "__main__":
    load_test()


```
> Configuration for discard extra terafic 

```
http {
        limit_req_zone $binary_remote_addr zone=one:10m rate=10r/s;
        server {
        location / {
        limit_req zone=one burst=2 nodelay;

}
}

```

- [ ] - On ELK side, create a retention mechanism to delete logs after X days/hours.

> Index Lifecycle Policy Fore Log Rotation 

```
PUT _ilm/policy/7-days-rotate
{
  "policy": {
    "phases": {
      "hot": {
        "min_age": "0ms",
        "actions": {
          "rollover": {
            "max_primary_shard_size": "50mb",
            "max_age": "7d"
          }
        }
      },
      "warm": {
        "min_age": "2d",
        "actions": {
          "shrink": {
            "number_of_shards": 1
          },
          "forcemerge": {
            "max_num_segments": 1
          }
        }
      },
      "delete": {
        "min_age": "7d",
        "actions": {
          "delete": {
            "delete_searchable_snapshot": true
          }
        }
      }
    },
    "_meta": {
      "managed": true,
      "description": "built-in ILM policy using the hot and warm phases with a retention of 7 days"
    }
  }
}
```

- [ ] - Write bash scripts for purging one file from cache storage.

```
#!/bin/bash

# Replace the path with your cache directory path
CACHE_DIR="/var/www/html/static/"

# Replace the file name with the name of the file you want to purge
FILE_NAME="file_name"

# Purge the file from the cache
grep -lr $FILE_NAME $CACHE_DIR | xargs rm -f

echo "File $FILE_NAME has been purged from Nginx cache."


```
- [ ] - Add some information in the HTTP response to inform the client about server name, cache status, and response
time

```
vim /etc/nginx/sites-available/reverse-proxy.conf

add_header X-Proxy-Cache $upstream_cache_status;
add_header Server "$hostname";
add_header X-Response-Time $request_time;
```
