<h2>  Install Nginx as a proxy to forward web requests to the webserver  </h2>

Note : In this case we used nginx as a proxy for forward client request and caching static content. 

<h3> Server Info </h3>

- Ip :  192.168.56.12, 192.168.58.12 
- Hostname : cdn.e2acloud.local
- Ram : 2G
- Cpu : 1 Core 

<h3> ISP Router [192.168.58.11] <--------BGP--------> [192.168.58.12] Edge Server  </h3>

---------
- [ ] - Step 1 : Pre Installation Task
- [ ] - Step 2 : Install Nginx and Config as a proxy a and caching 
- [ ] - Step 3 : Configure BGP  Edge Server Side
- [ ] - Step 4 : Install filebeat for send access and Error log to Log server 
---------
<h3> Step 1 : Pre Installation Task </h3>

> Install Packages 

```
$ sudo apt update -y 
$ sudo apt install nginx

```
---------
<h3> Step 2 : Install Nginx and Config as a proxy a and caching  </h3>

```
$ sudo unlink  /etc/nginx/sites-available/default
$ sudo rm -rf   /etc/nginx/sites-enable/default
$ sudo vim /etc/nginx/sites-available/reverse-proxy.conf

server {
    listen 80;
    server_name e2acloud.local;
    error_log /var/log/nginx/e2acloud.local_error.log;
    access_log /var/log/nginx/e2acloud.local_access.log;

    add_header Server "$hostname";      # respone server name in client request header 
    add_header X-Response-Time $request_time;   # respone time in client request header 

    location ~* \.(gif|otf|jpg|jpeg|png|css|js|ttf)$ {
        proxy_pass http://192.168.56.13;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_cache custom_cache;
        proxy_headers_hash_max_size 512;
        proxy_cache_valid any 10m;
        add_header X-Proxy-Cache $upstream_cache_status;    # show cache status , Hit or Miss 
        add_header Server "$hostname";
        add_header X-Response-Time $request_time;
    }

}
```
> Check Config and Restart nginx   

```
$ sudo nginx -t 
$ sudo systemctl enable  nginx 
$ sudo systemctl restart nginx 

```
---------
<h3> Step 3 : Configure BGP Side Of Edge Server  </h3>

> Enable Ip Forward and Routing 

```
echo 1 > /proc/sys/net/ipv4/ip_forward

```

```
$ sudo apt install bird  
$ sudo systemctl enable  bird 

$ sudo vim /etc/bird/bird.conf

log syslog all;
router id 192.168.58.12;

protocol kernel {
        scan time 10;
        import none;
        export all;   # Actually insert routes into the kernel routing table
}
protocol device {
        scan time 10;
}

protocol direct {
        interface "lo";               # Disable by default
}
protocol static {
        route 192.168.0.1/32 via 192.168.58.11;     # sample we can use loopback ip in production env
        route 192.168.56.12/32 via 192.168.58.11;   
        route 192.168.57.0/24 via 192.168.58.11;
        route 192.168.57.11/32 via 192.168.58.11;

}

protocol bgp neighbor_v4_1 {
  local as 65001;
  neighbor 192.168.58.11 as 65002;      # next hop ISP 
  export all;
  import all;
}

```
```
$ sudo systemctl restart bird 
```

> You must see this output

```
root@cdn:~# birdc show protocols all neighbor_v4_1
BIRD 1.6.8 ready.
name     proto    table    state  since       info
neighbor_v4_1 BGP      master   up     2023-06-24  Established
  Preference:     100
  Input filter:   ACCEPT
  Output filter:  ACCEPT
  Routes:         2 imported, 5 exported, 0 preferred
  Route change stats:     received   rejected   filtered    ignored   accepted
    Import updates:              2          0          0          0          2
    Import withdraws:            0          0        ---          0          0
    Export updates:              5          0          0        ---          5
    Export withdraws:            0        ---        ---        ---          0
  BGP state:          Established
    Neighbor address: 192.168.58.11
    Neighbor AS:      65002
    Neighbor ID:      192.168.58.11
    Neighbor caps:    refresh enhanced-refresh restart-aware llgr-aware AS4
    Session:          external AS4
    Source address:   192.168.58.12
    Hold timer:       195/240
    Keepalive timer:  58/80

```
* local as: Specifies the local AS number.
* neighbor: Specifies a BGP neighbor.
* import: Specifies a BGP import filter.
* export: Specifies a BGP export filter.
* multihop: Specifies the number of hops to a BGP neighbor.
* hold time: Specifies the hold time for a BGP session.


---------
<h3> Step 4 : Install filebeat for send access and Error log to Log server  </h3>

> Install filebeat

```
$ sudo curl -fsSL https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
$ sudo echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list
$ sudo apt update
$ sudo apt install  filebeat
```
> Configure filebeat 

```
$ sudo vim /etc/filebeat/filebeat.yml

```
> Add This Configuration to file 

```
# ---------------------------- Elasticsearch Output ----------------------------
output.elasticsearch:
  # Array of hosts to connect to.
  hosts: ["192.168.56.14:9200"]                # traget Server (Log server)

# ------------------------------ Nginx e2acloud  Output -------------------------------
- type: log
paths: - /var/log/nginx/e2acloud.local_access.log*
exclude_files: [".gz$"]
fields:
codec: plain
token: USER_TOKEN
type: nginx_access
fields_under_root: true
- type: log
paths: - /var/log/nginx/e2acloud.local_error.log*
exclude_files: [".gz$"]
fields:
codec: plain
token: USER_TOKEN
type: nginx_error
fields_under_root: true

```
> Enable module nginx 

```
$ sudo filebeat modules enable nginx
$ sudo filebeat setup -e
```
> restart Service 

```
$ sudo systemctl enable filebeat
$ sudo systemctl restart  filebeat
```
