<h2>  Install ELK for gathering Nginx access/error logs in different elastic search indices  </h2>

Note : In this case we used ELK for manage log from nginx server (cache server)

<h3> Server Info </h3>

- Ip :  192.168.56.14
- Hostname : elk.e2acloud.local
- Ram : 3G
- Cpu : 2 Core 

<h3> Edge Server [192.168.56.12] <--------filebeat--------> [192.168.56.14] ELK Server </h3>

---------
- [ ] - Step 1 : Pre Installation  Task
- [ ] - Step 2 : Install ELK and Configuration
- [ ] - Step 3 : Explotre kibana and Defaine Some Configuration 
---------
<h3> Pre Installation  Task </h3>

- [ ] - Installing and Configuring Elasticsearch

```
$ sudo curl -fsSL https://artifacts.elastic.co/GPG-KEY-elasticsearch |sudo gpg --dearmor -o /usr/share/keyrings/elastic.gpg
$ sudo echo "deb [signed-by=/usr/share/keyrings/elastic.gpg] https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list
$ sudo apt update -y 
$ sudo apt install elasticsearch -y 
```
---------
<h3> Install ELK and Configuration </h3>

```
$ vim /etc/elasticsearch/elasticsearch.yml
network.host: localhost
http.host: 0.0.0.0
http.port: 9200

```
> Enable and start service elasticsearch

```
$ sudo systemctl start elasticsearch
$ sudo systemctl enable elasticsearch
```

- [ ] - Installing and Configuring the Kibana Dashboard

> Install packages

```
$ sudo apt install kibana nginx 
$ sudo systemctl enable kibana
$ sudo systemctl start kibana
$ sudo systemctl enable nginx 
$ sudo systemctl start nginx 
```
> Configure Kibana Access 

```
$ sudo echo "kibanaadmin:`openssl passwd -apr1`" | sudo tee -a /etc/nginx/htpasswd.users

password : 1234
```
> Configure Nginx for kibana acess 

```
$ sudo vim /etc/nginx/sites-available/elk.e2acloud.local.conf

server {
    listen 80;

    server_name elk.e2acloud.local;

    auth_basic "Restricted Access";
    auth_basic_user_file /etc/nginx/htpasswd.users;

    location / {
        proxy_pass http://localhost:5601;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
}
```
> Create link new file

```
ln -s /etc/nginx/sites-available/elk.e2acloud.local.conf /etc/nginx/sites-enabled/elk.e2acloud.local.conf
```
> Restart Nginx and Reload Configuration

```
$ sudo nginx -t
$ sudo systemctl reload nginx

```
> Check Access to dashboard 

```
$ sudo curl -I  -k -u kibanaadmin:1234    http://elk.e2acloud.local/status
```
<h3> Install and Configuration filebeat </h3>

```
sudo apt install filebeat
sudo vim  /etc/filebeat/filebeat.yml

```
> Edit file beat 

```
vim /etc/filebeat/filebeat.yml

...
output.elasticsearch:
  hosts: ["localhost:9200"]


```
> Enable Nginx  module 

```
sudo filebeat modules enable
sudo filebeat modules list
sudo filebeat setup --pipelines --modules nginx
sudo filebeat setup --index-management -E output.logstash.enabled=false -E 'output.elasticsearch.hosts=["localhost:9200"]'
sudo filebeat setup -E output.logstash.enabled=false -E output.elasticsearch.hosts=['localhost:9200'] -E setup.kibana.host=localhost:5601

sudo systemctl start filebeat
sudo systemctl enable filebeat

curl -XGET 'http://localhost:9200/filebeat-*/_search?pretty'



```




---------

<h3> Explotre kibana and Defaine Some Configuration </h3>

> Index Lifecycle Policy Fore Log Rotation 

```
PUT _ilm/policy/7-days-rotate
{
  "policy": {
    "phases": {
      "hot": {
        "min_age": "0ms",
        "actions": {
          "rollover": {
            "max_primary_shard_size": "50mb",
            "max_age": "7d"
          }
        }
      },
      "warm": {
        "min_age": "2d",
        "actions": {
          "shrink": {
            "number_of_shards": 1
          },
          "forcemerge": {
            "max_num_segments": 1
          }
        }
      },
      "delete": {
        "min_age": "7d",
        "actions": {
          "delete": {
            "delete_searchable_snapshot": true
          }
        }
      }
    },
    "_meta": {
      "managed": true,
      "description": "built-in ILM policy using the hot and warm phases with a retention of 7 days"
    }
  }
}
```
